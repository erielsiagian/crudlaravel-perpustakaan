<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Books;
use App\Category;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = DB::table('books')->paginate(15);
        $category = DB::table('category');
        return view('index',['books'=>$books, 'category'=>$category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validateData = $request->validate([
            'buku_id' => 'required',
			'buku_id_category' => 'required',
			'buku_nama' => 'required',
        ]);


        DB::table('books')->insert([
			'buku_id' => $request->buku_id,
			'buku_id_category' => $request->buku_id_category,
			'buku_nama' => $request->buku_nama,
        ]);
        

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $validateData = $request->validate([
            'buku_id' => 'required',
			'buku_id_category' => 'required',
			'buku_nama' => 'required',
        ]);

        
        $books = DB::table('books')->where('buku_id',$id)->get();
		return view('edit',['books' => $books]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('books')->where('book_id',$request->id)->update([
			'buku_id' => $request->buku_id,
			'buku_id_category' => $request->buku_id_category,
			'buku_nama' => $request->buku_nama,
		]);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('books')->where('book_id',$request->id)->delete();
        return redirect('/');
    }
}
