<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>CRUD Perpustakaan</title>
    </head>
    <body>
            <div class="content">
                <div><h1>CRUD Tabel</h1></div>

            <table class="table table-bordered">
                <thead>
                <td>ID Buku</td>
                <td>ID Kategori</td>
                <td>Nama Buku</td>
                <td>Kategori</td>
                <td><a href="/create">Tambah</a></td>
                </thead>
                <tbody>
                @foreach($books as $p)
                <td>{{$p->buku_id}}</td>
                <td>{{$p->buku_id_category}}</td>
                <td>{{$p->buku_nama}}</td>
                <td>{{$p->getCategory()}}</td>
                <td><a href="/edit/{{ $p->buku_id }}" >Edit</a></td>
                <td><a href="/destroy/{{ $p->buku_id }}" onclick="return confirm('Anda akan menghapus data ini.')">Hapus</a></td>
                @endforeach
                </tbody>
            </table>
            </div>  
    </body>
</html>
