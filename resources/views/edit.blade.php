<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>CRUD Perpustakaan</title>
    </head>
    <body>
            <div class="content">
            <div><h1>Data Buku</h1></div>


                <a href="/pegawai"> Kembali</a>
	
	            <br/>
 
	            @foreach($books as $p)
	            <form action="/store" method="post">
		        {{ csrf_field() }}
		        Nomor ID Buku <input type="text" name="buku_id" value="{{$p->buku_id}}"> <br/>
		        Nomor ID Kategori <input type="text" name="buku_id_category" value="{{$p->buku_id_category}}"> <br/>
		        Nama Buku <input type="number" name="buku_nama" value="{{$p->buku_nama}}"> <br/>
		        <input type="submit" value="Create Data Buku">
				@endforeach


				ID Kategori
            	<select name="buku_id_category" id="cars" onchange="this.form.submit()">
            	@foreach($category as $c)
            	<option value="{{$c->category_name}}">{{$c->category_id}}</option>
           		@endforeach
            	</select>
	            </form>

				<h1>Tabel ID Kategori</h1>
             	<table class="table table-bordered">
                <thead>
                <td>Nama Kategori</td>
                <td>ID Kategori</td>
                </thead>
                <tbody>
                @foreach($category as $b)
                <td>{{$b->category_name}}</td>
                <td>{{$b->category_id}}</td>
                @endforeach
                </tbody>
	           
				
    </body>
</html>
