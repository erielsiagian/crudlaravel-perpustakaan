<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>CRUD Perpustakaan</title>
    </head>
    <body>
            <div class="content">
            <div><h1>Data Buku</h1></div>


           <a href="/"> Kembali</a>
 
	        <form action="/store" method="post">
		    {{ csrf_field() }}
		    Nomor ID Buku <input type="text" name="buku_id"> <br/>
		    Nama Buku <input type="number" name="buku_nama"> <br/>

            ID Kategori
            <select name="buku_id_category" id="cars" onchange="this.form.submit()">
            @foreach($category as $c)
            <option value="{{$c->category_name}}">{{$c->category_id}}</option>
            @endforeach
            </select>

            <a href="/create/category">Tambah Data Kategori</a>
		    <input type="submit" value="Create Data Buku">
	        </form>

            <h1>Tabel Kategori</h1>
             <table class="table table-bordered">
                <thead>
                <td>Nama Kategori</td>
                <td>ID Kategori</td>
                </thead>
                <tbody>
                @foreach($category as $b)
                <td>{{$b->category_name}}</td>
                <td>{{$b->category_id}}</td>
                @endforeach
                </tbody>
            </table>
            </div>

    </body>
</html>
